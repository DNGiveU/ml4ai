package com.ml4ai.nn.core.optimizers;

import com.ml4ai.nn.core.Variable;

public interface NNOptimizer {

    void update();

    void initialize(Variable... parameters);

}
