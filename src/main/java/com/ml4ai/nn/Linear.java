package com.ml4ai.nn;

import com.ml4ai.nn.core.Variable;
import org.nd4j.linalg.factory.Nd4j;

public class Linear implements ForwardNetwork {

    private Variable weights;
    private Variable bias;

    public Linear(int input, int output) {
        weights = new Variable(Nd4j.randn(new int[]{input, output}));
        bias = new Variable(Nd4j.zeros(new int[]{output}));
    }

    @Override
    public Variable[] getParameters() {
        return new Variable[]{weights, bias};
    }

    @Override
    public Variable[] forward(Variable... inputs) {
        return new Variable[]{inputs[0].matMul(weights).addVec(bias)};
    }
}
